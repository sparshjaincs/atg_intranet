from django.shortcuts import render,redirect
from django.http import HttpResponse
from INTRANET.models import People
from django.contrib import messages
# Create your views here.
def index(request):
	return render(request,'INTRANET/login.html')
def index1(request):

	if(request.POST):
		login_data = request.POST.dict()
		global Email
		user = login_data.get("username")
		password = login_data.get("pass")
		x=People.objects.all()
		for i in range(len(x)):
			if user==x[i].Username:
				if password==x[i].Password:
					flag=1
					break
				else:
					flag=0
					messages.info(request,'Password Is Incorrect!!')
			else:
				flag=0
				messages.info(request,'Username Is Incorrect!!')
		if flag==1:
			return render(request,'INTRANET/home.html',{'User':user,'password':password})
		else:
			return redirect("/")
def forgot(request):
		return render(request,'INTRANET/forgot.html')
def reset(request):
	flag=0
	if request.POST:
		data = request.POST.dict()
		user = data.get('username')
		password=data.get('password')
		conf_pass=data.get('pass')
		z=People.objects.all()
		for i in range(len(z)):
			if z[i].Username==user:
				if password == conf_pass:
					x=People.objects.get(Username=user)
					x.Password=password
					x.save()
					flag=1
					messages.info(request, 'Your password has been changed successfully!')
				else:
					messages.info(request, 'Passwords does not match!!')
					flag=0
			else:
				messages.info(request, 'Username Is Invalid!!')

	if flag==1:
		return redirect('/')
	else:
		return render(request,'INTRANET/forgot.html')