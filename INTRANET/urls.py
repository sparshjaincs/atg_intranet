from django.urls import path
from . import views

urlpatterns = [
    path('', views.index,name="login"),
    path('submit/',views.index1,name="home"),
    path('Forgot_password/',views.forgot,name="forgot"),
    path('Forgot_password/reset/',views.reset,name="forgot")
]
