from django import forms
class Login(forms.Form):
    Email=forms.EmailField(label="Email",max_length=100,widget=forms.EmailInput,required=True,help_text="Email")
    Password=forms.CharField(max_length=100,widget=forms.PasswordInput(),required=True,help_text="Password")
    Contact=forms.CharField(required=True,help_text="Contact")
    def __init__(self, *args, **kwargs):
        super(Login, self).__init__(*args, **kwargs)
        self.fields['Email'].widget.attrs['placeholder'] = 'Email'
        self.fields['Password'].widget.attrs['placeholder'] = 'Password'
        self.fields['Contact'].widget.attrs['placeholder'] = 'Contact'